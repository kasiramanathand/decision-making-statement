/*************
JavaScript Coding Challenge 2
*/
var teamJohn, teamMike,teamMary;

teamJohn = (89+120+103) / 3;
teamMike = (116+94+123) / 3;
teamMary = (97+134+105) / 3;
console.log(teamJohn, teamMike, teamMary);

if(teamJohn > teamMike && teamJohn > teamMary){
    console.log('The Winner is teamJohn and the average score is ' + teamJohn);
}
else if(teamJohn < teamMike && teamMary < teamMike){
    console.log('The Winner is teamMike and the average score is ' + teamMike);
}
else if(teamJohn < teamMary && teamMike < teamMary){
    console.log('The Winner is teamMary and the average score is ' + teamMary);
}
else{
    console.log('The is a draw');
}